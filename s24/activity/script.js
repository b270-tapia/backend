/*
	3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.

*/
let num = 2;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

const address = ["258 Washington Ave NW", "California", "90011"];
const [streetNumber, city, zipCode] = address
console.log(`I live at ${streetNumber}, ${city}, ${zipCode}`);

let animal = {
	name: "Lolong",
	type: "salwater crocodile",
	weigth: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name, type, weigth, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weigth} with a measurement of ${measurement}`);

let numArr = [1, 2, 3, 4, 5];

numArr.forEach(number => console.log(number));


let reduceNumber = () => {
	let total = numArr.reduce((prev, cur) => prev + cur);
	console.log(total);
}

reduceNumber();

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed
	}
}

let myDog = new Dog("Frankie", 5, "Miniature Dachshund")
console.log(myDog);