// console.log("Hello World!");

//[SECTION] ES6 Updates
	// ES6 also known as ECMAScript 2015 is an update to the previous versions of ECMAScript
	// ECMA - European Computer Manufacturers Association
	// ECMAScript is the standard that is used to create implementations of the language, one is the JavaScript

// New Features of JavaScript

// [SECTION] Exponent Operator
const anotherNum = 8 ** 2;
console.log(anotherNum);


// Using Math Objects methods
const	num = Math.pow(8 ,2);
console.log(num);

// [SECTION] Templates Literals

// pre-tempplate literals
// using single/double quotes(""/ '')
let name = "Glenn";

let message = "Hello " + name + "! \n Welcome to programming";
console.log(message);

//String using templates literals
// Uses backticks (``) instead of ('') or ("")

message = `Hello ${name}! Welcome to programming`;
console.log(`Message with template literals: ${message}`);

// Multi-line using template literals
const anotherMessage = `${name} attented a math competition.
He won it by  solving the problem 8 ** 2 with the solution of ${anotherNum}.`
console.log(anotherMessage);

/*
	-Template Litera;s allow us to write strings with embedded JavaScript expressions
	- "${}"  are used to incloud JS expressions
*/

const interestRate = .5;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructuring
/*
	- it allow us to unpack elements in arrays into distinct variables
	- allow us to name array elements with variables instead of using index numbers
	- Syntax:
	let/const [variableNameA, variableNameB, variableNameC] = array
*/

const fullName = ["Glenn", "Tanael", "Tapia"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(fullName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// [SECTION] Object Destructuring

let person = {
	givenName: "Jane",
	midName: "Dela",
	familyName: "Cruz"
}

console.log(person.givenName);
console.log(person.midName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.midName} ${person.familyName}! it's good to see you!`);

let {givenName, midName, familyName} = person;

console.log(givenName);
console.log(midName);
console.log(familyName);

console.log(`Hello ${givenName} ${midName} ${familyName}! it's good to see you!`);

//[SECTION] Arrow Function
/*
	- Compact alternative to traditional fucntions
	- This will only work with "function expressions"
	- Syntax:
		let/const variableName = () => {
			code block/statement
		}
		let/const variableName = (parameter) => {
			code block/statement
		}
*/
//function declaration
function greeting(){
	console.log("Hello World!");
}
greeting();

//function expression
const greet = function(){
	console.log("Hi, World!");
}
greet();

// Arrow Function

const greet1 = () => {
	console.log("Hello Again!");
}
greet1();

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName("John", "D", "Smith");

// Arrow Function with Loops
const students = ["John", "Jane", "Judy"];

// Pre-arrow function
students.forEach(function(student){
	console.log(`${student} is a student.`);
})
//Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`);
})

// An arrow function has an implicit return value when the curly braces are omitted


const add = (x, y) => {		//same output
	return x + y;			// const add = (x, y) => x + y;
}
	 
let total = add(1, 2);
console.log(total);

//[SECTION] Default Fucntion Argument Value

const greet2 = (name = "User") => {
	return `Good Morning ${name}!`;
}
console.log(greet2());
console.log(greet2("Zoie"));

// [SECTION] Class-Based Object Blueprints

/*
	-"constructor" is a special method of a class for creating/initializing an object for that class
	-Syntax:
		class className {
			constructor(objectPropertyA, objectPropertyB) {
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectPropertyB
			}
		}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Honda";
myCar.name = "Civic";
myCar.year = "2022";
console.log(myCar);

// Instantiating a new object with initialized values

const myNewCar = new Car("Toyota", "Fortuner", "2020");
console.log(myNewCar);