// console.log("Happy Wednesday!");

// Array Methods
	// Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items

// [Mutator Methods]
	/*
		- These are methods that "mutate" or change an array after they are created
		-These methods manipulate the original array performing various tasks such as adding and removing elements
	*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
// push()
	/*
	-Adds an element at the end of an array AND returns the new array's length
	- Syntax:
		arrayName.push(elementToBeAdded);

	*/

console.log("Current fruits array");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);	//5 - arrays length
console.log("Mutated array from push method:");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop()
	/*
		-removes the last element in an array AND returns the removed element
		-Syntax:
			arrayName.pop();
	*/

let removedFruit = fruits.pop();
console.log(removedFruit);

console.log("Mutated array from pop method");
console.log(fruits);

// unshift()
	/*
		-Adds one or more elements at the beginning of an array AND returns the new array
		-Syntax:
			arrayName.unshift(elementA, elementB);
	*/
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

// shift()
	/*
		-Removes an element at the beginning of an array and returns the removed element
		-Syntax:
			arranName.shift();
	*/
let anotherFruit = fruits.shift();
console.log(anotherFruit); //Lime
console.log("Mutated array from shift method");
console.log(fruits);

// splice()
	/*
		-Simultaneously removes elements from a specified index number and adds elements.
		-Returns the removeds element/s
		-Syntax:
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
	*/

let fruitSplice = fruits.splice(1, 2, "Lime", "Cherry");
console.log(fruitSplice); //Apple and Orange - removed elemets
console.log("Mutated array from splice method");
console.log(fruits);

// sort()
	/*
		-Rearranges the array elements in alphanumeric order
		-Syntax:
			arrayName.sort();
	*/

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse()
	/*
		-Reverses the order of the array elements
		-Sytax:
			arrayName.reverse();
	*/

fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);


// Non-Mutator Methods
	/*
		-methods that do not modify or change an array after they are created
		-do not manipulate the original array performing various tasks such as retuning elements from an array and combining arrays and printing outputs
	*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);

// indexOf()
	/*
		-Retuns the index nuber of the first matching element found in an array
		-if not match was found, it will return -1
		-The search process will start from the first element proceeding to the last element
		-Syntax:
			arrayName.indexOf(searchvalue)
	*/

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex); //1

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry); //-1

// lastIndexOf()
	/*
		-Returns the index of the last matching element found in the array
		-The search process will be done from the element proceeding to the first element.
		-Syntax:
			arrayName.lastIndexOf(searchvalue)
	*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex); //5

// slice()
	/*
		-Portions/slices elements from an array and returns a new array
		-Syntax:
			arrayName.slice(starting index);
	*/

//Slicing of elements from a specified index to the last element
let sliceArrayA = countries.slice(2);
console.log("Result of slice method: ");
console.log(sliceArrayA); // ['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

// Slicing off elemnts  starting from specified index to another index
let sliceArrayB = countries.slice(2, 5);
console.log("Result of slice method: ");
console.log(sliceArrayB); //['CAN', 'SG', 'TH']

// Slicing of elemetns starting from the last element of an array
let sliceArrayC = countries.slice(-3);
console.log("Result of slice method: ");
console.log(sliceArrayC); //['PH', 'FR', 'DE']

// toString()
	/*
		-Returns an array as a string separated by commas
	*/
let stirngArray = countries.toString();
console.log("Result from toString method: ");
console.log(stirngArray); //US,PH,CAN,SG,TH,PH,FR,DE

// concat()
	/*
		-combune arrays and returns the combines result/ array
		-Syntax:
			arrayName.concat(arrayB)
	*/

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breath bootstrap"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method: ");
console.log(tasks);

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// combining arrays with elements
let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log("Result from concat method: ");
console.log(combinedTasks);

// join()
	/*
		-returns an array as a string separated by a specified separator
		-Syntax:
			arrayName.join("seperator")
	*/
let users = ["John", "Jane", "Joe", "Joshua"];
console.log(users.join());
console.log(users.join(" "));
console.log(users.join(" - "));

// [SECTION] Iteration Methods

// forEach()
	/*
		-similar to a for loop that iterates on each array element
		-for each item in the array, the anonymous function passed in
		the forEach() will be run
		-the anonymous functions is able to receive the curent item being iterated or looped over by assign parameter
		-Syntax:
			arrayName.forEach(function(inidivElement){
				statement
			})

	*/

let filteredTasks = [];
allTasks.forEach(function(task) {
	if(task.length > 10) {
		filteredTasks.push(task);
	}
})

console.log("Result of forEach: ");
console.log(filteredTasks);

// map()
	/*
		-iterates on each element and returns a new array
		-Syntax:
			arrayName.map(function(indivElement){
				statement
			})
	*/
let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number) {
	return number * number;
})
console.log("Original Array: ");
console.log(numbers);
console.log("Result of map methods: ");
console.log(numberMap);


// map() vs forEach()

let numbersForEach = numbers.forEach(function(number) {
	return number * number;
})
console.log(numbersForEach); //undefined - because forEach() does not return anything

// every()
	/*
		-checks if all elements in an array meet the given conditions
		-returns a true value if all elements meet the condition and false if otherwise
	*/

let allValid = numbers.every(function(number){
	return (number < 3);
})
console.log("Result of every method: ");
console.log(allValid);

// some()
	/*
		-checks if at least one element in the array meets the given condition
		-returns a true value if some elements meet the condition and false if otherwise
	*/

let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log("Result of some method: ");
console.log(someValid);

// filter()
	/*
		- returns a new array that contains the elements which meet the given condition
		- returns an empty array if no elements were found
		
	*/
let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log("Result of filter method: ");
console.log(filterValid);

// includes()
	/*
		-checks if  the argument passed can be found in the array
		-returns a boolean
	*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1); // true

let productFound2 = products.includes("Headset");
console.log(productFound2); // false

// reduce()
	/*
		-evaluates elements from left to right and returns/reduces array into a single value
	*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current iteration: " + ++iteration);
	console.log("Accumulator: " + x);
	console.log("currentvalue: " + y);

	return x + y;
})
console.log("Result of reduced method: " + reducedArray);

let list = ["Hello", "Again", "World"];
// 1. x = Hello, y = Again => Hello Again
// 2. x = Hello Again + y = World => Hellow Again World - reduced value

let reducedString = list.reduce(function(x, y){
		return x + " " + y;
})

console.log("Result of reduce method: " + reducedString); //Hello Again World