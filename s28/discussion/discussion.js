// CRUD Operationgs

/*
	- CRUD operations are the heart of backend applications
	- mastering the CRUD operations of any langauge makes a valuable developer and makes the works easier for us to deal with huge amount of information
*/

// [SECTION] Inserting documents (CREATE)
/*
	Sytax:
		db.collectionName.insertOne({Object})
		db.collectionName.insertMany([{ObjectA}, {ObecjtB}])
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"

})

// Inset Many
db.users.insertMany([

		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "0987654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"

		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "0987654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"

		}

	]);

// [SECTION] Finding documnets (READ)
/*
	Syntax:
		db.users.find();
		db.users.find({ field: value });
*/
// Leaving the search criteria empty will retrieve ALL the documents
db.users.find();

// Finding single document
db.users.find({ firstName: "Stephen" });
db.users.find({ age: 82 });

// Finding documnets with multiple parameters
/*
	Syntax:
		db.users.find({ fieldA: valueA, fieldB: valueB });
*/
db.users.find({ lastName: "Armstrong", age:82 });
db.users.find({ firstName: "Jane", age: 21 });

// [SECTION] Updating documents (UPDATE)


db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})

db.users.updateOne(
	{firstName: "test"},
	{
		$set: {firstName: "Glenn"}
	}

)
// Updating document
/*
	- just like the "find" method, methods
	-Syntax:
		db.collectionName.updateOne({criteria}, {$set: {filed : value}});

*/

db.users.updateOne(
	{firstName: "test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
)
db.users.find({ firstName: "Bill" });
// Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria}, {$set: {field: value}})
*/
db.users.updateMany(
	{department: "none"},
	{
		$set:
		{
			department: "HR"
		}
	}
)

// Replace One
/*
	can be used if replacing the whole document is necessary
*/

db.users.replaceOne(
	{firstName: "Bill"},
		{
		
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",	
		}
)

db.users.replaceOne(
		{firstName: "Glenn"},
		{
			lastName: "Tapia",
			age: 10,
			department: "Operations"
		}

	);

// Deleting documents (DELETE)

// document to delete
db.users.insertOne({
	firstName: "test"
})

db.users.deleteOne()

// deleting single document
/*
	Syntax:
		db.users.deleteOne({criteria})
*/
db.users.deleteOne({firstName: "test"});

// Delete Many

/*
	- Be careful when using the "deleteMany". If no search criteria is provided, it will delete all the dox inside
*/
db.users.deleteMany({firstName: "Bill"})

// [SECTION] Advanced Queries

// Query on embedded documnet
// regards the order/ exact elements
db.users.find({
	contact : {
		phone: "0987654321",
		email: "stephenhawking@gmail.com"
	}
})

db.users.find({
	"contact.email" : "janedoe@gmail.com"
})

// Query on array
// regardless of order
db.users.find({
	courses: ["CSS", "JavaScript", "Python"]
})

db.users.find({
	courses: { $all : ["React", "Python"]}
})

// Query on embedded array

db.users.insertOne({
	namearr: [
			{
				namea: "juan"
			},
			{
				nameb: "tamad"
			}
		]
})

db.users.find({
	namearr: 
		{
			namea: "juan"
		}
})