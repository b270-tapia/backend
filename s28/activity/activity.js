db.rooms.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	roomsAvailable: 10,
	isAvailable: false

});

db.rooms.insertMany([
		{
			name: "double",
			accomodates: 3,
			price: 2000,
			description: "A room fit for a small family going on a vacation",
			roomsAvailable: 5,
			isAvailable: false
		},
		{
			name: "quenn",
			accomodates: 4,
			price: 4000,
			description: "A room with a quenn sized bed perfect fo a simple getaway",
			roomsAvailable: 15,
			isAvailable: false
		}
	]);

db.rooms.find({ name: "double" });

db.rooms.updateOne(
		{name: "quenn"},
		{
			$set: {roomsAvailable: 0}
		}
	);

db.rooms.deleteMany({ roomsAvailable: 0 });