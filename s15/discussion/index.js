// alert("Hello, Batch 270!");

// This is a stament.
console.log("Hello again!");
console.log("Hi, Batch 270!");









//  [SECTION] Variables
	/*
		-It is used to contain data
	*/

// Declaring Variables
// Syntax: let/const varaiableName;

/*
	- Tring to print out a value of a variable that has not been declared will return an error of "undefined"
	- Variables must be declared first with value before they can be used
*/
	let myVariable;
	console.log(myVariable);

	let	greeting = "Hello";
	console.log(greeting);

// Declaring and initializing variables
// Initializing variables - instance when a variable is given its initial value
// Syntax: let/const varaiableName = value;

let productName = 'desktop computer';
productName = "laptop"
console.log(productName);

let	productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

let friend = "Kate";
console.log(friend);


//  we cannot re-declare variables within its scope
let friend1 = "Jay";
console.log(friend1);

// [SECTION] Data Type

// String - series of characteres that create a word a phrase, a sentence or anything related to creating a text
// Strings can be written using a single or double quote.
let country = "Philippines";
let province = "Batangas";
console.log(country);
console.log(province);

// Concatenating strings
// Multiple String values can be combined to create a single string using "+" symbol
let fullAddress = province + ", " + country;
console.log(fullAddress);
console.log("I live in the " + country);

// \n refers to creating a new line in between text
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress)

let message = "John's employees went home early.";
console.log(message);

// Numbers
// integers/whole numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers
let grade = 98.7;
console.log(grade);

// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// Boolean - value is either true or false

let isMarried = false;
let inGoodConduct = true;
console.log("isMarriend: " +isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays - are special kind of data types used to store multiple
// Arrays can store different data types but is normally used to store similar types
// Syntax: let/const arrayName = [elementA elements...]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912345678", "09876654321"],
	address: {
		housenumber: "345",
		city: "Manila"
	}
};
console.log(person);

let grades1 = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6,
}
console.log(grades1);

// Null
let spouse = null;
console.log(spouse);