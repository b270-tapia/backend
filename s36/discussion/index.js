// setup dependencies
const express = require("express");

const mongoose = require("mongoose");

const taskRoutes =  require("./routes/taskRoutes")

// Server setup
const app = express();
const port = 3001; 

// Database connection

mongoose.connect("mongodb+srv://admin:admin123@zuitt.3aoorzt.mongodb.net/b270-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended : true}));
// Add thetask route
// Allow all the task routes created in the "taskRoutes.js" file to use "/tasks" routes
app.use("/tasks", taskRoutes)


// Server listening

app.listen(port, () => console.log(`Server running at  ${port}`) );