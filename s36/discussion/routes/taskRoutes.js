// Contains all the endpoints for our application

const express = require("express");

// The "taskController" allows us to use the functions defined in the "taskController.js"
const taskController = require("../controllers/taskController");

// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// [SECTION] Routes

// Route to get all the tasks
router.get("/", (req, res) => {

	// Invokes the "getAllTasks" function from the "taskController.js" file
	// "resultFromController" is only used here to make the code easier to understand but it's common practice to use the shorthand parameter name for a result using the parameter name "result"/"res"
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete a task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// Route to update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Activity

router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id, req.body).then(resultFromController => res.send (resultFromController));
})

router.put("/:id/complete", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Exports the router object to be used in the "index.js"
module.exports = router;