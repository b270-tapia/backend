// fetch ("https://jsonplaceholder.typicode.com/todos")
// .then(response => response.json())
// .then(response => console.log(response));

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "GET",
	headers: {
		"Content-Type": "application/json"
	}
})
.then(response => response.json())
.then(response => console.log(response));

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "GET",
	
})

.then(response => response.json())

.then(response => {

	   const title =response.map(post => post.title)
	   console.dir(title)
	  
});


fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "GET",
    headers: {
		"Content-Type": "application/json"
	}
})

.then(response => {
    console.log(response.status);
    return response.json();
})
.then(response => console.log(`${response.title}`));


fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		completed: false,
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
		
	})
})
.then(response => response.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
.then(response => response.json())
.then(response => console.log(response));







