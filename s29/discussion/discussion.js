// Comparison Query Operator

// [ SUB-SECTION ] $gt/gte Operator

	// Allows us to find documents that have field number values greater than or equal to a specific value.

		 /*
			 Syntax:
				 db.collectionName.find({ field: { $gt: value } });
				 db.collectionName.find({ field: { $gte: value } });
		 */


db.users.find({ age : { $gt : 50 } });
db.users.find({ age : { $gte : 50 } });

// [ SUB-SECTION ] $lt/$lte Operator

	// Allows us to find documents that have field number values less than or equal to a specified value. 
	
		 /*
			 Syntax:
				 db.collectionName.find({ field: { $lt: value } });
				 db.collectionName.find({ field: { $lte: value } });
		 */

db.users.find({ age : { $lt : 50 } });
db.users.find({ age : { $lte : 50 } });

// [ SUB-SECTION ] $ne Operator

	// Allows us to find document that have field number values not equal to a specified value.

		 /*
			 Syntax:
				 db.collectionName.find({ field: { $ne: value } });
		 */

db.users.find({ age : { $ne: 82 } });

// [ SUB-SECTION ] $in Operator

	// Allows us to find documents with specific match criteria one field using different values
		
		// Syntax:
			// db.collectionName.find({ field: { $in : value } });

db.users.find({ lastName: { $in : ["Hawking", "Doe"] }});
db.users.find({ courses: { $in : ["HTML", "React"] }});



//  Logical Query Operator

// [SECTION] $or Operator

	
	// Allows us to find documents that match a single criteria from multiple provided search criteria//

	/*
		Syntax: 
		 	db.collectionName.find({ $or: [{ fieldA : valueA }, { fieldA : valueA } ]});
	*/ 

db.users.find( {$or: [{ firstName: "Neil" }, { age: 25 } ]});
db.users.find( {$or: [{ firstName: "Neil" }, { age: { $gt: 30 } } ]});

// [SECTION] $and Operator

	// Allows us to find documents matching criteria in single field

		/*
			Syntax:
				db.collectionName.find({ $and : [{ fieldA : {valueA}}, {fieldB { valueB } }]});
		*/

db.users.find({ $and : [{ age: { $lt : 90} }, { age : { $gt : 80} }]});

// Field Projection
	/*
		-Retrieving are common opertaions that we do by default MongoDB queries return the whole document as a response.
		-when dealing with complex data structures, there might be instances when field are not useful for the query that we are trying to accomplish.
	*/

// [SECTION] Inclusion

	/*
		-allows us to include/add specific field only when retrieving documents.
		-the values provided is 1 to denote that the field is being included.

		Syntax:
			db.collectionName.find({ criteria }, {field: 1});
	*/

db.users.find({firstName: "Jane"}, {firstName:1, lastName: 1, contact: 1});

db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		}

	);

// [SECTION] Exclusion
	
	/*
		-Allow us to exclude/remove specific field only when retrieving documents.
		- The provided is 0 to denote that the field is being excluded.

		Syntax:
			db.collectionName.find({ criteria }, {field: 0});
	*/
db.users.find(
		{firstName: "Jane"},
		{contact: 0, department: 0}
	);

// Supressing the ID Field

	/*
		- allow to exclude "_id" when retrieving documnets.

		Syntax:
			db.collectionName.find({criteria}, {_id : 0 });
	*/

db.users.find(
		{firstName : "Jane"},
		{
			firstName : 1,
			lastName : 1,
			contact : 1,
			_id :0
		}
	);

// [SECTION] Returning specific fields in embedded documnets

db.users.find(
		{ firstName: "Jane" },
		{
			firstName: 1,
			lastName: 1,
			"contact.phone": 1
		}
	);

// [SECTION] Supressing specific fields in embedded documnets
db.users.find(
		{ firstName: "Jane" },
		{
			
			"contact.phone": 0
		}
	);

// Evaluation Query Operators

// [SECTION] $regex Operator

	/*
		- Allows us to find documnets that match a specific string pattern using regular expression.
		Syntax:
			db.collectionName.find({ field: $regex : 'pattern', $options: 'optionValue'});
	*/

// Case Sensitive Query
db.users.find({ firstName: {$regex: 'n'}});

// Case Insensitive Query
db.users.find({ firstName: {$regex: 'j', $options: 'i'} });