// console.log("Hello World");

// Functions
	// Functions in Javascript are lines/blocks of codes that can tell our device or application to perform a certain task when called or invoked.

// Function Declarations
	// function statement - defines a function with a specified parameter

	/*Syntax:
		function functionName() {
			code block (statement)
		}

		-function keyword - used to define a JS function
		-fuctionName - is the function's name. Functions are named to be able to be used to called later in our code.
		-function block ({}) - the statements which comprise the body of the function. This is where the code to be executed is written.
			
	*/

	function printName() {
		console.log("Hi! My name is John.")
	};

	// Function Invocation.
	printName();

	// declaredFunction(); - result to an error. Uncaught ReferenceError: declaredFunction is not defined. not yet declared or not yet defined.
	// semicolons are used to separate executable JS statement or codes.


// Function Declaration vs Function Expressions

	// Function Declaration
	declareFunction(); //declared functions can be hoisted. As long as the fuinction has been declared.

	function declareFunction() {
		console.log("Hello World from declareFunction")
	};

	declareFunction();

	// Function Expression

		// A function can also be stored in a variable.
		// A funvtion expression is anonymous function assigned to the variableFunction

		/*
			let variableName = function() {
				codeblock (statement)
			}
		*/
		// let n = 30; // - this is how we initialize a variable

		// variableFunction(); this will result to an error: Uncaught ReferenceError: Cannot access 'variableFunction' before initialization.

		let variableFunction = function() {
			console.log("Hello again Batch270!")
		};

		variableFunction();

		let funcExpression = function funcName() {
			console.log("Hello from the other side")
		};

		funcExpression();

		// You can reassign declared functions and fucntion expressions to a new anonymous functions.

		declareFunction = function() {
			console.log("Updated declareFunction")
		};

		funcExpression = function() {
			console.log("Updated funcExpression")
		};

		declareFunction();
		funcExpression();

		const constantFunc = function() {
			console.log("Initialize with const")
		};

		constantFunc();
		declareFunction();

		/*constantFunc = function() {
			console.log("Initialize with const")
		};
		constantFunc();*/

	// Function Scoping

		/*
			Scope is the accessibility (vissibility) of varirables within our program

				Javascript Variables has 3 types of scope:
					1. local/block scope
					2. global scope
					3. function scope
		*/

		// Local Scope
		{
			let localVar = "Armando Perez";
			console.log(localVar);
		}


		// Global Scope
		let globalVar = "Mr. World";

		console.log(globalVar);
		// console.log(localVar);

		// Fuction Scope
		function showNames() {
			// fucntion scoped variables
			var functionVar = "Joe";
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);
		}
// 
		showNames();

		// console.log(functionVar);
		// console.log(functionConst);
		// console.log(functionLet);


// Nested Function
		// We can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable name, as they are within the same scoope/code block

		function myNewFunction(){
			let name = "Jane";

			function nestedFunction() {
				let nestedName = "John";
				console.log(name);
				console.log(nestedName);
			}

			nestedFunction();
		}
		// nestedFunction(); - this one results to an error
		myNewFunction();
		// Function and Global Scoped Variable

		let globalName = "Alexandro"
		function myNewFunction2(){
			let nameInside = "Renz"
			console.log(globalName);
		}
		myNewFunction2();

// Using alert()

		// Syntax: alert("<messageInString>");
		alert("Hello World!"); // This will run immediately when the page loads

		function showSampleAlert(){
			alert("Hello, User!");
		}
		// showSampleAlert();

		console.log("I will only in the console when the alert is dismissed.");

// Using Prompt()

		// Syntax: prompt("<dialougeInString");
		let samplePrompt = prompt("Enter your name.");
		console.log("Hello; " + samplePrompt);
		console.log(typeof samplePrompt);

		let sampleNullPrompt = prompt("Don't enter anything");
		console.log(sampleNullPrompt);

		function printWelcomMessage(){
			let firstName = prompt("Enter your first name.");
			let lastName = prompt("Enter your last name.");

			console.log("Hello, " + firstName + " " + lastName + "!")
			console.log("Welcome to my page!");
		}
		printWelcomMessage();

// Function Naming Conventions


		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"];
		}
		getCourses();

		// 2. avoid generic names to avoid confussion within your code.

		function get(){
			let name = "Jamie";
			console.log(name);
		}
		get();

		// 3. Avoid pointless and inappropriate function names

		function foo() {
			console.log(25%5);
		}
		foo();

		// 4. Name your functions using camecasing

		fucntion displayCarInfo() {
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");
		}
		displayCarInfo();