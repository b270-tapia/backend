/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function customerInfo(){
		let fullName = prompt("What is your name?");
		let age = prompt("How old are you?");
		let location = prompt("Where do you live?")

		console.log ("Hello, " + fullName);
		console.log ("You are " + age + " years old.")
		console.log ("You live in " + location);
	}

	customerInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function myFavoriteArtists() {
		let top1 = "Queen";
		let top2 = "Boys 2 Men";
		let top3 = "My Chemical Romance"
		let top4 = "Simple Plan";
		let top5 = "The All-American Rejects";
		console.log("1. " + top1);
		console.log("2. " + top2);
		console.log("3. " + top3);
		console.log("4. " + top4);
		console.log("5. " + top5);
	}

	myFavoriteArtists();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	

*/
	
	//third function here:
	function myFavoriteMovies() {
		let top1 = "Demon Slayer - Kimetsu no Yaiba - The Movie: Mugen Train";
		let top2 = "Avatar (2009)";
		let top3 = "Game of Thrones"
		let top4 = "The First Slam Dunk";
		let top5 = "That Time I Got Reincarnated as a Slime the Movie: Scarlet Bond";
		console.log("1. " + top1);
		console.log("Rotten Tomatoes Rating: 98%")
		console.log("2. " + top2);
		console.log("Rotten Tomatoes Rating: 82%")
		console.log("3. " + top3);
		console.log("Rotten Tomatoes Rating: 88%")
		console.log("4. " + top4);
		console.log("Rotten Tomatoes Rating: 94%")
		console.log("5. " + top5);
		console.log("Rotten Tomatoes Rating: 57%")
	}
	myFavoriteMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

