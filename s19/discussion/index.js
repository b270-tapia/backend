// console.log("Happy Friday!");

//What are conditional statements?
	// allow us to control the flow of our program. They also allow us to run statements/instructions if a conditions is met or run another separate instruction if otherwise.

// [SECTION], if, else if and else statement

let numA = -1;


//  if statement - executes if a specified conditions is true
/*
Syntax:
	if(condition){
		statement
	}
*/
if(numA < 0){
	console.log("Hello");
}

console.log(numA < 0);

let city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City!");
} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}

// else if statement
	// executes statement if previous conditions are false and if the specified condition is true
let numH = 1;
if(numH < 0){
	console.log("Hello again!");
} else if (numH > 0){
	console.log("World");
}

// else statement

if (numA > 0){
	console.log("Hello");
} else if (numA === 0){
	console.log("World");
} else {
	console.log("Again");
}

/*else {
	console.log("Hi, Batch 270!"); - will result to an error
}*/ 

// if, else if and else statements with functions

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30){
		return "Not a typhoon yet.";
	} else if (windSpeed <= 61) {
		return "Trophical despression detected."
	} else if (windSpeed >= 62 && windSpeed <=88) {
		return "Tropical storm detected.";
	}else if (windSpeed >=89 || windSpeed <=117){
		return "Severe tropical storm detected.";
	}else{
		return "Tyhpoon detected.";
	}

}
let message = determineTyphoonIntensity(110);
console.log(message);

if(message == "Severe tropical storm detected.") {
	console.warn(message);
}

// [SECTION] Truthy and Falsy
	

// Truthy Examples
if (true) {
	console.log("Truthy");
}

if (1) {
	console.log("Truthy");
}

if ([]) {
	console.log("Truthy");
}

// Falsy Examples
	// Falsy Values
		// 1. ""
		// 2. null
		// 3. NaN

if (false) {
	console.log("Falsy");
}
if (0) {
	console.log("Falsy");
}
if (undefined) {
	console.log("Falsy");
}

// [SECTION] Conditional teranry Operator
	// takes in three operands
	// 1.condition
	// 2.expression to be executed if the condition is truthy
	// 3.expression to be executed if the condition is falsy

	/*
		Syntax:

		(expression) ? ifTrue :  ifFalse;

	*/
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

let name;
function isOfLegalAge() {
	name = "John";
	return	"You are of legal age limit";
}

function isUnderAge() {
	name = "Jane";
	return "You are under the age limit"
}

// parseInt() " converts the inputs receive from the prompt into a number data type"
let age = parseInt(prompt("What is your age?"));
console.log(age);

let	legalAge = (age > 17) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary operator in functions: " + legalAge + ", " + name);

// [SECTION] Switch Statement

/*
	Syntax:
	switch (expression) {
	case value:
		statement;
		break;
	default:
		statement;
	}

*/
// .toLowercase() will change the input received from the prompt into all lowercase ensuring a match with the switch cases if the user inputs capitalized or uppercased
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case "monday":
		console.log("The color of the day is red.")	;
	break;

	case " tuesday":
		console.log("The color of the day is orange.");
		break;

	case "wednesday":
		console.log("The color of the day is yellow.");
		break;	

	case "thursday":
		console.log("The color of the day is green.");
		break;

	case "friday":
		console.log("The color of the day is blue.");
		break;

	case "saturday":
		console.log("The color of the day is violet.");
		break;

	case "sunday":
		console.log("The color of the day is violet.");
		break;

	default:
		console.log("Please input a valid day");
}

// [SECTION] Try-Catch-Fianlly

function showIntensityAlert(windSpeed) {
	try {
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch (error) {
		console.log(typeof error);
		console.warn(error.mesage);
		console.warn(error.name);
	}
	finally {
		alert ("Intensity updates will show new alert.");
	}
}
showIntensityAlert(56);