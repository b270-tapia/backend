// console.log("Hello World!");

//[SECTION] JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages
	- JSON is used for serializing different data types into types
	- Serialization is the process of converting data into a series of bytes for easier transmission or transfer of information
	-Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/

// JSON Objects

// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// // JSON Arrays

// "cities": [
// 		{
// 			"city": "Quezon City",
// 			"province": "Metro Manila",
// 			"country": "Philippines"
// 		},
// 		{
// 			"city": "Manila City",
// 			"province": "Metro Manila",
// 			"country": "Philippines"
// 		},
// 		{
// 			"city": "Makati City",
// 			"province": "Metro Manila",
// 			"country": "Philippines"
// 		}
// 	]

// [SECTION] Jason Methods
// The JSON object contains methods for parsing and converting data into stringified JSON


// [SECTION] Converting data into strigified JSON
let batchesArr = [
		{
			batchName: "Batch 270"
		},
		{
			batchName: "Batch 271"
		}
	]

console.log(batchesArr);

// The "stringify" method is to convert JS object into string
console.log(`Result of stringify method`);
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
		name: "John",
		age: 31,
		address: {
			city: "Manila",
			country: "Philippines"
		}
	});
console.log(data);

// User details

// let firstName = prompt("What is your first name?");
// let lasttName = prompt("What is your last name?");
// let age = prompt("How old are you?");
// let address = {
// 	city: prompt("Which city do you live in?"),
// 	ccountry: prompt("Which coutry does your city belong to?")
// };

// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lasttName: lasttName,
// 	age: age,
// 	address: address
// })
// console.log(otherData);

// [SECTION] Converting Stringified JSON into JS Objects
let batchesJSON = `[
	{
		"batchName": "Batch 270"
	},
	{
		"batchName": "Batch 271"
	}
]`;
console.log(`Result from parse method: `);
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "John",
	"age": "31",
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}

}`
console.log(JSON.parse(stringifiedObject));