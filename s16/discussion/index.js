// console.log("Hello World");

// Arithmetic Operators

	let x = 10;
	let y = 12;

	let	sum = x + y
	console.log("Results of addition operator: " + sum);

	let difference = x - y;
	console.log("Results of subtraction operator: " + difference);

	let product = x * y;
	console.log("Results of multiplication operator: " + product);

	let quotient = y / x;
	console.log("Results of division operator: " + quotient);

	let	remainder = y % x;
	console.log("Results of modulo operator: " + remainder);

// Assignment Operators

	// Basic Assignment Operators(=)
	let	assignmentNumber = 8;

	// Addition Assignment Operators(+=)

	// assignmentNumber = assignmentNumber + 2;
	// console.log("Results of addition assignment operator: " + assignmentNumber);

	// shorthand for assignmentNumber = assignmentNumber + 2;
	assignmentNumber += 2;
	console.log("Results of addition assignment operator: " + assignmentNumber);

	assignmentNumber -= 2;
	console.log("Results of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Results of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Results of division assignment operator: " + assignmentNumber);

// Multiple Operators

	//  The operations were done in the following order: MDAS rule
	// 1. 3 * 4 = 12
	// 2. 12 / 5 = 2.4
	// 3. 1 + 2  = 3
	// 4. 3 - 2.4 = 0.6

	let	number = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operator" + number);

	//  The operations were done in the following order: PEMDAS rule
	// 1. 4 / 5 = 0.8
	// 2. 2 - 3 = -1
	// 3. -1 * 0.8 = -0.8
	// 4. 1 + -0.8 = 0.2
	

	let pemdas = 1 + (2 - 3) * 4 / 5;
	console.log("Result of pemdas operator" + pemdas);

// Type Coercion

	let numA = "10";
	let numB = 12;

	let coercion = numA + numB
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;
	let noCoercion = numC + numD;
	console.log(noCoercion);
	console.log(typeof noCoercion)

	// the result is a number
	// the "true" is also associated with the value of 1
	let numE = true + 1;
	console.log(numE)

	// the "false" is also associated with the value of 0
	let numF = false + 1;
	console.log(numF);

// Comparison Operators

	let juan = "juan";

	// Equality Operator (==)
	/*
		-Checks whether the operands are equal/have the same content
		-Returns a boolean value
	*/

	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == "1"); //true
	console.log(0 == false); //true 
	console.log("juan" == "juan"); //true
	console.log("juan" == juan); //true

	// Inequality Operator

	console.log("Inequality Operator")
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != "1"); //false
	console.log(0 != false); //false 
	console.log("juan" != "juan"); //false
	console.log("juan" != juan); //false

	// Strict Equality Operator
	/*
		-Checks whether the operands are equal/have the same content
		-Returns a boolean value
		-Also compares the data types of the 2 values
	*/
	console.log("Strict Equality Operator")
	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === "1"); //false
	console.log(0 === false); //false 
	console.log("juan" === "juan"); //true
	console.log("juan" === juan); //true

	console.log("Strict Inequality Operator")
	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== "1"); //true
	console.log(0 !== false); //true 
	console.log("juan" !== "juan"); //false
	console.log("juan" !== juan); //false

// Relational Operators

	let a = 50;
	let b = 65;

	console.log("Relational Operators")
	// Greater than operator ( > )
	let isGreaterThan = a > b;
	console.log(isGreaterThan); //false

	// Less than operator ( < )
	let isLessThan = a < b;
	console.log(isLessThan); //true

	// GTE ( >= )
	let isGtorEqual = a >= b;
	console.log(isGtorEqual); //false

	// LTE ( <= )
	let isLtorEqual = a <= b;
	console.log(isLtorEqual); //true

// Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND Operator (&&)
	// Returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND Operator: " + allRequirementsMet); //false

	// Logical OR Operator(||)
	// Returns true if one of the operands is turue
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR Operator: " + someRequirementsMet); //true

	// Logical NOT Operator (!)
	// Returns the Opposite Value 
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT Operator: " + someRequirementsNotMet); //true

// Increment and Decrement

		// Operators that add or subtract
	let z = 1;

	// pre-increment

	// The value of "z" is added by values of 1 before returning the value and storing it in the variable
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	// Post-increment
	// The value of "2" is returned first and stored in the variable "increment" then the value of "z" is increased by 1
	increment = z++
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	// Pre-decrement
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	// Post-decrement
	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);