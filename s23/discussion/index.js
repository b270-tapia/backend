// console.log("Welcome back, Batch 270!");

// [SECTIONS] Objects
	/*
		-An object is data types is used to represent a real world objects
		-Information is stored and represented in a "key-value" pair
	*/


// creating objects using initializers/literal notation
/*
	-This creates/declares an object and also initializes/assigns its properties upon creation
	-A cellphone is an example of a real world objects
	-It has its own properties such as name, color, price, weight, unit, etc.
	-Syntax
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};
console.log("Result from creating object using intializers/literal notation:");
console.log(cellphone);
console.log(typeof cellphone);

// Creating Objects using a consumer function

/*
	-Creates a reusable function to create several objects that have same data structure
	-An "instance" is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
	-Syntax:
		function ObjectName(keyA, keyB) {
			this.keyA = keyA,
			this.keyB = keyB,

		}
*/

// The "this" keyword allows us to assign a new object's property by associating them with values received from a constructopr function's parameter
function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// This is a unique instance of the laptop object 
// The "new" operator creates an instance
let laptop = new Laptop("Lenovo", 2008);
console.log("Results from creating objects using object constructor: ")
console.log(laptop);

// This is a another unique instance of the laptop object 
let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Results from creating objects using object constructor: ")
console.log(myLaptop);


// This only invokes/calls the "Laptop" functions instead of creating a new object instance
let oldLaptop = Laptop("Acer", 1980);
console.log("Results from creating objects without using the new keyword: ")
console.log(oldLaptop); // undefined

// Creating empty objects
let computer = {};
console.log(computer);

let myComputer = new Object();
console.log(myComputer);

//  [SECTION] Accessing object properties

// Using the dot notation
console.log("Result from dot notation: " + myLaptop.name); //Macbook Air

// Using square bracket notation
console.log("Results from square bracket notation: " + myLaptop["name"]);

// [SECTION] Initializing/Adding/Deleting/Reassigning Object properties

let car = {};
console.log(car);

// Initializing/adding object properties using dot notation
car.name = "Honda Civic";
console.log("Results from adding properties using do notation: ")
console.log(car);

// Initializing/adding object properties using squarebracket notation
car["manufactureDate"] = 2020;
console.log("Results from adding properties using squarebracket notation: ")
console.log(car);

// Reassigning Object properties

car.name = "Ford Raptor";
console.log(car);

// Deleting object properties
delete car["manufactureDate"];
console.log("Result from deleting properties: ");
console.log(car);

// [SECTION] Objects Method

let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name);
	}
}
console.log(person);
console.log("Results from object methods: ");
person.talk();

// Adding methods to objects
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}
person.walk();
console.log(person)

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas"
	},
	emails: ["joe@mail.com", "joesmith@email.xyz"],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName);
	}
}
friend.introduce();

/*
	Scenario:
	1.We would like to create a game that would have several pokemon interaction with each other
	2.Every pokemon would have the same set of stats, properties, adn functions
*/

// Create multiple kinds of pokemon
// Using object literals to create multiple pokemons would be time consuming
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonhealth");
	},
	faint: function (){
		console.log("Pokemon fainted");
	}
}
console.log(myPokemon);


// Object constructor

function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonhealth");
	}
	this.faint = function(){
		console.log(this.name + " fainted.");
	}
}

// Creates new instance of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
console.log(pikachu);

let rattata = new Pokemon("Rattata", 8);
console.log(rattata);
pikachu.tackle(rattata);
rattata.tackle(pikachu);