let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
	
}
console.log(trainer);


console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation: ")
console.log(trainer["pokemon"]);
console.log("Result of talk method: ")
trainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		targetPokemonhealth = target.health - this.attack;
		target.health = targetPokemonhealth;
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + targetPokemonhealth);

		
		if(target.health <= 0){
			target.faint();

		}
		console.log(target);
	}
	this.faint = function(){
		console.log(this.name + " fainted.");
	}
}


let pikachu = new Pokemon("Pikachu", 12)
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8)
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100)
console.log(mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);
