db.fruits.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplier_id: 1,
		onSale: true,
		origin: [ "Philippines", "US"]
	},

	{
		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplier_id: 2,
		onSale: true,
		origin: [ "Philippines", "Ecuador"]
	},	

	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplier_id: 1,
		onSale: true,
		origin: [ "US", "China"]
	},	

	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplier_id: 2,
		onSale: false,
		origin: [ "Philippines", "India"]
	},	
]);

// MongoDB Aggregation

	/*
		- used to generate manipulated data and perform opertaions to create filtered results that helps in analyzing data.
	*/

// [SECTION] Using the Aggregate Method

	/*
		- the  $match is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process.

		Syntax:
		{ $match: { field: value }}

		Syntax:
		{ $group : { _id: "value", fieldResult: "valueResult"}}

		- using the $match and $group along with aggregation will find for products that are on sale and will group the total amount of stock for suppliers found.

		Syntax:
		db.collectionName.aggregate({
			{ $match: { field: value } },
			{ $group : { _id: "$value", fieldResult: "valueResult"}}
		});

		- "$" symbol will refer to field name that is available in the documnets that are being aggregated.
	*/

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: {_id: "$supplier_id", total: { $sum: "$stock"} }}
]);

// [SECTION] Field Projection with Aggregation

	/*
		- the $project can be used when aggregating data to include/exclude fields from the return results

		Syntax:
			{ $project : { field: 1/0 }}
	*/

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: {_id: "$supplier_id", total: { $sum: "$stock"} }},
	{ $project: { _id: 0 } }
]);

// [SECTION] Sorting Aggregated Results

	/*
		- The sort can be used to change the order of aggregated results
		- Providing a value of -1 will sort the aggregated results in a reverse order

		Syntax:
			{ $sort: { _id: 1/-1} }
	*/

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: {_id: "$supplier_id", total: { $sum: "$stock"} }},
	{ $sort: { total: -1 } }
]);

// [SECTION] Aggregating Results Based on Array Fields

	/*
		- The $unwind deconstruct an array field from a collection/field with an array value to output a result for each element

		Syntax:
			{ $unwind: "field" }
	*/

db.fruits.aggregate([
		{ $unwind: "$origin" }
	]);
		// Display fruits documents by their origin and the kind of fruits that are supplied

db.fruits.aggregate([
		{ $unwind: "$origin" },
		{ $group: { _id: "$origin", kinds: { $sum : 1} }}
	]);