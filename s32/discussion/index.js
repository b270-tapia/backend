let http = require("http");
let port = 4000;
let server = http.createServer(function (request, response) {
	// HTTP Method of incoming request can be accessed via "method" property in request parameter
	// "GET" means that we will be retreiving/reading information
	if(request.url == "/items" && request.method == "GET"){
		// Requests the "/items" path and "GETS" information 
		response.writeHead(200, {'Content-Type': 'text/plain'});
		// Ends the response process
		response.end('Data retrieved from the database');
	};

	if (request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Data to be sent to the database')
	}
})
server.listen(port);
console.log(`Server running at localhost:${port}`);