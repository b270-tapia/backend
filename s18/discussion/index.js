// console.log ("Happy Thursday, Batch 270!");

// Fucntion Parameters and Arguments

	// We learned that we can gather data from user input using prompt()

	function printInput() {
		let nickname = prompt("Enter your nickname:");
		console.log("Hi! " + nickname)
	}
	// printInput();

	// Consider this function:
	function printName(name) {
		console.log("My name is " + name);
	}
	printName("Juana");
	printName("John");
	printName("Jane");

	// "name" - parameter
		// a parameter acts as container or named variable that exist only inside a function
		// "Juana" - argument
		// An argument is the information/data provided directly into the function
		// variables can also be passed as am argument
	let	sampleVariable = "Yui";
	printName(sampleVariable);

	function checkDivisibiltyBy8(num) {
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);

		let iskDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(iskDivisibleBy8);
	}

	checkDivisibiltyBy8(64);
	checkDivisibiltyBy8(28);

	// Functions are arguments

	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed.")
	}
	function invokeFunction(argumentFunction){
		argumentFunction();
	}
	invokeFunction(argumentFunction);

	// Multiple Parameters
	function createFullName(firstName, middleName, lastName){
		console.log (firstName + " " + middleName + " " + lastName);	
	}
	createFullName("Juan", "Dela", "Cruz");

	// In JS, providing arguments more/less arguments than the expected parameters will not return an error

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";
	createFullName(firstName, middleName, lastName)

// Return Statement

	function returnFullName(firstName, middleName, lastName){
		return firstName + " " + middleName + " " + lastName;
		// any line/block of code that comes after the retun statement is ignored because it ends the function execution
		console.log("This message will not be printed");
	}
	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName)

	// You can also create a variable inside the function to contain the result
	function returnAddress(city, country) {
		let fullAddress = city + ","+ country;
		return fullAddress;
	}
	let myAddress = returnAddress("Cebu City", "Philippines");
	console.log(myAddress);
	// Whatever value is returned from the the returnFullName function is stored in the completeName variable

	function printPlayerInfo(username, level, job){
		console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
	}

	let user1 = printPlayerInfo("knight_white", 95, "paladin");
	console.log(user1);


	// Other Sample

	function printSum(num1, num2) {
		return num1 + num2;
	}
	// printSum(5,5);

	function getProduct(num) {
		let product = num * 5;
		console.log(product);
	}

	getProduct(printSum(5,5));

	function getDifference (num1, num2) {
		return num1 - num2;
	}
	let difference = getDifference(10,5);
	console.log(difference);

	function getQuotient(num){
		let  quotient = num / 2;
		console.log(quotient);
	}

	getQuotient(difference);